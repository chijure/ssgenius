
<html lang="en">
                        

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/iconcool.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    AIW
  </title>
  
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/paper-dashboard.min790f.css?v=2.0.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="./assets/demo/demo.css" rel="stylesheet" />
  <link href="./ilcb/dist/css/mod.css" rel="stylesheet" />

</head>

<body class="">
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Google Tag Manager (noscript) -->

  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="#" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="./assets/img/iconcool.png">
          </div>
        </a>
        <a href="#" class="simple-text logo-normal">
         AIW
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="./assets/img/default-avatar.png" />
          </div>
          <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                <?php echo $nombre; ?>
                
              </span>
            </a>
            <div class="clearfix"></div>
            <div class="collapse" id="collapseExample">
              
            </div>
          </div>
        </div>
        <ul class="nav">
          <li class="active">
            <a href="#">
              <i class="nc-icon nc-bank"></i>
              <p>Panel</p>
            </a>
          </li>
        
         
      
         
         
         
          
          <li>
            <a href="logout.php">
              <i class="nc-icon nc-button-power"></i>
              <p>Salir</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-icon btn-round">
                <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
              </button>
            </div>
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#">Panel</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link btn-magnify" href="#pablo">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
            
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="#pablo">
                  <i class="nc-icon nc-settings-gear-65"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Account</span>
                  </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header">
  
  <canvas id="bigDashboardChart"></canvas>
  
  
</div> -->
      <div class="content">

            <div class="col-md-12">
            <div class="row">
                
                <!--
              <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                     <a href="./ilcb/formularios.php?grupo=ILCB"> <img src="./assets/img/isotipo.png" alt=""/></a>
                    </div>
                    <a href="./ilcb/formularios.php?grupo=ILCB">  <h3 class="card-title">ILCB</h3></a>
                    <ul>
                      <li>Instituto Le Cordon Blue</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div>
          
              
             <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                      <a href="#"> <img src="./assets/img/isotipo.png" alt=""/></a>
                    </div>
                    <a href="./pec/formularios.php?grupo=PEC"> <h3 class="card-title">PEC</h3></a>
                    <ul>
                      <li>Programas de Eucaci贸n Continua</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div>
              
              <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                      <a href="#"> <img src="./assets/img/isotipo.png" alt=""/></a>
                    </div>
                    <a href="./ulcb/formularios.php?grupo=ULCB"> <h3 class="card-title">ULCB</h3></a>
                    <ul>
                      <li>Universidad Le Cordon Blue</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div>


              <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                      <a href="#"> <img src="./assets/img/isotipo.png" alt=""/></a>
                    </div>
                    <a href="./master_cuisine/formularios.php?grupo=MASTER"> <h3 class="card-title">MASTER CUISINE</h3></a>
                    <ul>
                      <li>MASTER CUISINE</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div>
              
              <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                      <a href="consolaInformativa/"> <img src="./assets/img/mimir.png" height="200" /></a>
                    </div>
                    <a href="consolaInformativa/"> <h3 class="card-title">DASHBOARD</h3></a>
                    <ul>
                      <li>DASHBOARD</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div> -->
              <div class="col-lg-4">
              </div>
              
              <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">

                      <a href="./demo/reporteproductomedio15.php"> <img src="http://aiw-roi.com/sistema/AIW/assets/img/ACCESO-300x225.jpg" height="200"/></a>
                    </div>
                    <a href="./demo/reporteproductomedio15.php"> <h3 class="card-title">AIW - DEMO</h3></a>
                    <ul>
                      <li>DEMO</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div>

             <!-- <div class="col-lg-4">
                <div class="card card-pricing ">
                  <div class="card-header">
                    <h6 class="card-category"></h6>
                  </div>
                  <div class="card-body">
                    <div class="card-icon icon-primary ">
                      <div style="height: 200px">
                      <a href="#"> <img src="./assets/img/logomovil.jpg" /></a>
                    </div>
                    </div>
                    <a href="#"> <h3 class="card-title">MT</h3></a>
                    <ul>
                      <li>Movil Tours</li>
                    
                    </ul>
                  </div>
                 
                </div>
              </div> -->

            </div>
          </div>
      </div>
      <?php include('views/pie-pagina.php'); ?>
    </div>
  </div>
 
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="./assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="./assets/js/plugins/sweetalert2.min.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="./assets/js/plugins/jquery.validate.min.js"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="./assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="./assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="./assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--  Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="./assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="./assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="./assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="./assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Bootstrap Table -->
  <script src="./assets/js/plugins/nouislider.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="../../../buttons.github.io/buttons.js"></script>
  <!-- Chart JS -->
  <script src="./assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="./assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/paper-dashboard.min790f.js?v=2.0.1" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="./assets/demo/demo.js"></script>
  <!-- Sharrre libray -->
  <script src="./assets/demo/jquery.sharrre.js"></script>
 

</body>

</html>