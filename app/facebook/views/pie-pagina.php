<footer class="footer footer-white">
	<div class="container">
		<div class="row align-items-center flex-row-reverse">
			<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
				Copyright © 2018 <a href="https://ssgenius.com" target="_blank">SSG</a>. Developed with <i style="color: #eb5e28 !important; animation: n 1s ease infinite !important;"  class="nc-icon nc-favourite-28"></i> by <a href="http://aiw-roi.com/sistema/" target="_blank">AIW</a> & <a href="#">Renato Carabelli</a> All rights reserved.
			</div>
		</div>
	</div>
</footer>